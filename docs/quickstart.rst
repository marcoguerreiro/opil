.. _sec-quickstart:

Quickstart guide
================

This section contains a quickstart guide on how to get OPiL running in PLECS and how to implement your own models. For a more complete  discussion on how this example works, and how it can be adapted to new models, see :ref:`sec-plecs-example`.

.. note::
   
   The example provided in this guide uses Windows to emulate the embedded controller. Running this example on an actual controller requires only implementing the communication interface on the embedded target. See :ref:`sec-c-implementation-running-opil-target` for a discussion on how to run ``OPiL target`` on an embedded target.
   
Requirements
------------ 

#. PLECS
#. Make for Windows
#. A C compiler for Windows

Make for Windows
^^^^^^^^^^^^^^^^

If you don't have ``make`` for Windows, you can install it from `here <https://gnuwin32.sourceforge.net/packages/make.htm>`_. The same webpage contains intructions on how it can be installed. Don't forget to add ``make`` to your system environment variables.

C compiler for Windows
^^^^^^^^^^^^^^^^^^^^^^

If you don't have a C compiler for Windows, you can use, for example, `MinGW <https://sourceforge.net/projects/mingw/>`_ to install one. 

Running OPiL
------------

The easiest way to get OPiL running is to run the example provided in the `project's official repository <https://gitlab.rhrk.uni-kl.de/lrs/opil>`_. The example shows how to use PLECS to simulate a buck converter and Windows to emulate the embedded controller.

Building and running
^^^^^^^^^^^^^^^^^^^^

With the command prompt, navigate to the ``examples/buck_target_win`` folder, found in the project's root directory. Once inside the folder, run the ``make`` command. The result should look something like the following: 

.. code-block:: console
   
   $ opil\examples\buck_target_win>make
   if not exist build mkdir build
   gcc -c ../../ctlrif/ctlrif.c -I../../ -o build/ctlrif.o
   if not exist build mkdir build
   gcc -c ../../comm/win/targetCommWinSock.c -I../../ -o build/targetCommWinSock.o
   if not exist build mkdir build
   gcc -c ../../opiltarget.c -I../../ -o build/opiltarget.o
   if not exist build mkdir build
   gcc -c src/buckcontrol.c -I../../ -o build/buckcontrol.o
   gcc -c src/main.c -I../../ -o build/main.o
   gcc build/main.o build/ctlrif.o build/targetCommWinSock.o build/opiltarget.o build/buckcontrol.o -lws2_32 -o build/opiltarget.exe

Once ``make`` is executed, a folder called ``build`` is created. The ``make`` command creates an executable file, called ``opiltarget.exe``. Double-click on this file to execute it. A new window should pop-up, with the following message:

.. code-block:: console
   
   main: OPiL target module ready to run.
   
   main: Waiting for a connection...

Now, the target is running, and waiting for new connections. 

Inside ``examples/buck_target_win`` is a folder called ``plecs``. Inside it, there's a PLECS file, which contains the simulation model for this example. Open this file and run the simulation. The result should be as in :numref:`fig-quickstart-example-run`. 

.. figure:: images/quickstart/example_run.png
   :name: fig-quickstart-example-run
   :scale: 40%
   :align: center
   :alt: PiL example.
   
   Successful execution of OPiL with PLECS and Windows.

The code executed by the controller is in the ``examples/buck_target_win/src/buckcontrol.c`` file. 

Running your own models
-----------------------

To run your own models, it is necessary to properly set PLECS, the OPiL framework, and the controller. This sections explains the necessary step for each one. The steps can be summarized as:

- Setting the inputs and outputs of the C-script containing the ``OPiL host`` module in the PLECS model.
- Defining measurements, simulation data, control and controller data signals in the OPiL framework.
- Setting the IP address and port of the target in the host communication module of the OPiL framework.
- Implementing ``initialize`` and ``control`` functions for the controller.
- Setting the port in the target communication module of the OPiL framework.

PLECS
^^^^^

Running OPiL with PLECS requires a C-script containing the code that runs the ``OPiL host`` module. The code inside this C-script does not require any changes, and can be reused in any simulation model. It is only necessary to properly set the number of inputs and outputs of the C-script.

The first step is to define what are the signals that will be sent to the controller, and what are the signals that are expected to be received from the controller. Once this is defined, the input signals can be multiplexed into the C-script, and the output signals can be demultiplexed out of the C-script.  It is also important to define the sampling time of the C-script. This corresponds to the execution period of the controller.

In the provided example, there are three signals that are sent to the controller: a current measurement, a voltage measurement and a reference voltage. Two signals are expected to be received from the controller: the duty-cycle, and the execution time of the control algorithm. In the example, the C-script containing the ``OPiL host`` module
is located inside the ``OPiL (SFB)`` subsystem. The subsystem and the settings of the C-script block are shown in :numref:`fig-quickstart-plecs-opil-host`

.. subfigure:: AB
   :layout-sm: AB
   :gap: 8px
   :subcaptions: below
   :name: fig-quickstart-plecs-opil-host
   :class-grid: outline

   .. image:: images/quickstart/plecs-opil-host.png
      :scale: 40%
      :alt: OPiL host module as a C-script in PLECS.

   .. image:: images/quickstart/plecs-c-script-config.png
      :scale: 32%
      :alt: C-script settings

   ``OPiL host`` module as a C-script in PLECS, and the C-script settings.


OPiL framework
^^^^^^^^^^^^^^

After setting the inputs and outputs of the C-script block, the next step is to define these signals in the OPiL implementation.  These signals are defined as C structures in the `stypesBuck.h <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/config/stypesBuck.h>`_. file found in the ``config`` folder. 

In this file, four structures corresponding to measurements, simulation data, control signals and controller data are defined. 

.. note::
   
   It is important that the order in which the variables are defined is the same as the inputs of the C-script in the PLECS model. For example, the structure ``stypesMeasurements_t`` has two elements, ``i`` and ``v_out``. The signals that are multiplexed into the C-script (see :numref:`fig-quickstart-plecs-opil-host`) must follow the same order.

After defining the signals as C structures, the next step is to include this file in the `stypes.h <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/config/stypes.h>`_ file (also located in the ``config`` folder). Although it is possible to define the signal structures directly in the `stypes.h <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/config/stypes.h>`_ file, defining them in different files allows for better management of files for different models.

The last step is to set the communication module, which just requires setting the IP address and port of the target. These settings are found in the `hostCommWinSock.h <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/comm/win/hostCommWinSock.h>`_ file, located in the ``comm/win`` folder.

Controller
^^^^^^^^^^

Setting the controller depends on the target used. In the example provided, Windows is used to emulate the controller. The ``OPiL target`` is initialized and executed in the `main <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/examples/buck_target_win/src/main.c>`_ file, found in the ``examples/buck_target_win/src`` folder. The control algorithm is set during this initialization (see line 24 in `main <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/examples/buck_target_win/src/main.c>`_), which requires that two controller functions are implemented: an initialization function and a control function.

An example of how to implement these functions and their signature is found in the `buckcontrol.c <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/examples/buck_target_win/src/buckcontrol.c>`_ and `buckcontrol.h <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/examples/buck_target_win/src/buckcontrol.h>`_  files. The ``initialize`` function is executed once when ``OPiL target`` receives a new connection, and the ``control`` function is executed once when new measurements and simulation data arrive from the host (simulation).

Implementing your own controller requires implementing initialization and control functions, with the same signature as shown in `buckcontrol.h <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/examples/buck_target_win/src/buckcontrol.h>`_.

The last step is to set the communication module, which just requires defining the port where ``OPiL target`` will run. The port is defined in the `targetCommWinSock.h <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/comm/win/targetCommWinSock.h>`_ file, located in the ``comm/win`` folder. 

.. raw:: html

    <hr>

This section presented a quickstart guide on how to get the example provided in the `project's official repository <https://gitlab.rhrk.uni-kl.de/lrs/opil>`_. For a more complete  discussion on how this example works, and how it can be adapted to new models, see :ref:`sec-plecs-example`.