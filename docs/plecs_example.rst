.. _sec-plecs-example:

PiL example with PLECS
======================

This sections shows an example of running a PiL scheme with OPiL. In this example, a buck converter is simulated with PLECS, and Windows is used as the target.

.. note::
   
   Although the controller is emulated with Windows, using an actual embedded target is just a matter of using a target with Ethernet, and implementing the communication interface for the target. The control algorithm and the controller interface remain exactly the same.

Model
-----

The simulation model consists of a buck converter, a PWM modulator, voltage and current sensors, and ADCs. From the controller's perspective, the converter can be seen as a system that takes as input the duty-cycle, and gives voltage and current measurements as outputs. A diagram of the model is shown in :numref:`fig-plecs-example-buck-model`, where the input to the system is the duty-cycle :math:`d`, and the outputs are the measurements :math:`i_\text{L}` and :math:`v_\text{o}`

.. figure:: images/plecs_example/buck_model.png
   :name: fig-plecs-example-buck-model
   :scale: 40%
   :align: center
   :alt: Buck model.
   
   Buck model used for PiL example.

Controller
----------

A state-feedback controller is used to control the converter. The control law :math:`d[k]` is given by

.. math::
   
   d[k] = k_\text{r}v^* - k_\text{i}i_\text{L} - k_\text{v}v_\text{o},

where :math:`v^*` is the reference, :math:`i_\text{L}` is the inductor current, :math:`v_\text{o}` is the output voltage, and :math:`k_\text{r}`, :math:`k_\text{i}` and :math:`k_\text{v}` are controller gains. 

The controller can be seen as a system that takes as inputs the reference, inductor current and output voltage, and gives a duty-cycle as output.

Diagram of PiL system
---------------------

A diagram of the PiL scheme used in this example is shown in :numref:`fig-plecs-example-pil-diagram`. The left side of the image shows what is inside the PLECS model. PLECS simulates the converter (power stage), as well as signal acquisition (sensors and ADCs) and actuation (PWM). The data is sent to a C-script block that runs the ``OPiL host`` module. The right side of the image shows the controller, which consists of the ``OPiL target`` module, and a ``buckcontrol.c`` source file. The source file is where the control law is executed.

.. figure:: images/plecs_example/pil_diagram.png
   :name: fig-plecs-example-pil-diagram
   :scale: 26%
   :align: center
   :alt: Diagram of the PiL scheme.
   
   Diagram of the PiL scheme.
   
Communication
-------------

Networks sockets are used as the communication medium. 

Running OPiL host in PLECS
------------------------------

``OPiL host`` must be executed by PLECS, which requires an interface between them. Interfacing ``OPiL Host`` with PLECS simply means providing ``OPiL Host`` with ways that allow getting and updating signals on PLECS' environment. 

In PLECS, this is achieved by using the ``C-script`` block. A complete description of how this block works is available in the official PLECS documentation. The C-script block can be viewed just like any regular block, containing inputs and outputs. However, this block allows executing C-code. Thus, this block is used as the interface between PLECS and C-code. The block  will not be explained in detail here, but rather how it was used.

As mentioned in the :ref:`sec-c-implementation-example-simif` section, the simulation interface needs to know where the data (measurements, simulation data, control signals, and controller data) are in the simulation. This is achieved by creating buffers in the C-script, which will hold these signals. Everytime the C-script block is executed, all inputs to the block (which are the measurements and simulation data) are copied to these buffers, and OPiL is executed. After execution, the output data (control and controller data) are copied to the output buffers automatically by OPiL, and the C-script block can read from these buffers and apply the signals as outputs of the block.

This idea is illustrated at a higher level in :numref:`fig-plecs-example-plecs-c-script-principle`, and at a lower level in :numref:`fig-plecs-example-plecs-c-script-principle-low-level`. 

.. figure:: images/plecs_example/plecs_c_script_principle.png
   :name: fig-plecs-example-plecs-c-script-principle
   :scale: 18%
   :align: center
   :alt: C-script in PLECS.
   
   C-script in PLECS.

:numref:`fig-plecs-example-plecs-c-script-principle` shows an example of a C-script block containing three input: two inputs are measurements (``i`` and ``v``); and one inputs is additional simulation data (``v_ref``). The block contains two outputs: the control signal (``u``) and additional controller data (``t_exec``).

:numref:`fig-plecs-example-plecs-c-script-principle-low-level` shows what happens inside the C-script block, at a lower level. At every sampling instant, the input signals of the C-script block are copied to internal buffers. These buffers are read by ``OPiL host``, and sent to the target through its communication interface. When ``OPiL host`` gets the control signals and controller data from the target, it copies them to the output buffers. These output buffers are used to update the output signals of the C-script block. This is how ``OPiL host`` accesses and updates data in the simulation side.

.. figure:: images/plecs_example/plecs_c_script_principle_low_level.png
   :name: fig-plecs-example-plecs-c-script-principle-low-level
   :scale: 15%
   :align: center
   :alt: Using the C-script to interface PLECS and OPiL host.
   
   Using the C-script to interface PLECS and OPiL host.

The C-script block is configured to have a sample time according to the sampling frequency of the controller. This  is not necessarily the same time step as the simulation, since the simulation may run with a variable step size. However, the C-script block is configured to execute with a regular sample time, since a digital controller would also run in such way.

The actual code inside the C-script block can be seen in the `example <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/examples/buck_target_win/plecs/opil_buck.plecs>`_ provided in the `project's official repository <https://gitlab.rhrk.uni-kl.de/lrs/opil>`_. 

In the example, the C-script block is located inside the ``OPiL (SFB)`` subsystem, and this is where ``OPiL Host`` is configured and executed. The C-script has several  code sections, which are executed at different moments. In this example, the ``code declarations``, ``start function code``, ``output function code``, and ``terminate function code`` are used. 

The ``code declarations`` section is kind of the "header" of the C-script. This section is used for "global" declarations. :numref:`fig-plecs-example-plecs-code-declarations` shows how it looks like in this example. The header and source files required to execute OPiL are included in lines 3-11. In lines 18-21, four buffers are created, to hold the data as specified by OPiL (measurements, simulation data, control signals, and controller data). The size of these buffers are automatically obtained by a structure that defines how the data look like (explained later in this section).

.. note::
   
   All paths shown in :numref:`fig-plecs-example-plecs-code-declarations` are relative, and will work only if the PLECS model is located in ``examples/buck_target_win/plecs``. If you wish to save your PLECS model in another directory, you have to modify the path. The path can also be absolute. 

An integer variable ``conn`` is created to hold the status of the connection between the simulation and the target (line 23). All OPiL related functions are only executed if the connection is successfully established.

.. figure:: images/plecs_example/plecs_code_declarations.png
   :name: fig-plecs-example-plecs-code-declarations
   :scale: 55%
   :align: center
   :alt: Code declarations.
   
   Code declarations.

The next section is ``start function code``. This section is executed only once, when the simulation is first started. This section is used to initialize the modules of ``OPiL host``, as shown in :numref:`fig-plecs-example-plecs-start-code`. First, the simulation interface is initialized (line 1). This initialization consists of giving the addresses of the buffers holding the input and output data (refer to the :ref:`sec-c-implementation-example-simif` section).

After the simulation interface is initialized, the OPiL host module is initialized. The structures that hold all callbacks are created in lines 3-4, and initialized in lines 6-16. OPiL is initialized in line 18. After initialization, an attempt to connect to the target is made (line 20). The variable ``conn`` holds the result of the connection. If its value is 0, it indicates that the connection was successfully made. As network sockets are used, it is necessary that the proper IP address and port are set in the communication interface (see the :ref:`sec-c-implementation-example-comm` section).

.. figure:: images/plecs_example/plecs_start_function_code.png
   :name: fig-plecs-example-plecs-start-code
   :scale: 55%
   :align: center
   :alt: Start function code.
   
   Start function code.

The next section is ``output function code``, shown in :numref:`fig-plecs-example-plecs-output-function-code`. This section is executed at each sampling instant (set in the C-script block settings). It is in this section where all data exchange with the target happens. First, all code in this section is only executed if a connection to the target was successfully made. If the connection is valid, the measurements and simulation data are copied to the internal buffers created inside the C-script (lines 5-13). This is where data is transferred from simulation to OPiL.

After copying the measurements and simulation data, ``opilhostExchangeDataTarget`` is used to execute OPiL's state machine, where data is sent to the target, and control signals and additional data are retrieved, and copied to their respective buffers. After ``opilhostExchangeDataTarget`` is executed, data from the ``control`` and ``controllerdata`` buffers are copied to the output of the C-script block. This is how OPiL updates the signals in the simulation.

.. figure:: images/plecs_example/plecs_output_function_code.png
   :name: fig-plecs-example-plecs-output-function-code
   :scale: 55%
   :align: center
   :alt: Output function code.
   
   Output function code.

The last section is ``terminate function code``, shown in :numref:`fig-plecs-example-plecs-terminate-function-code`. This section is executed once when the simulation is finished. In this example, this section is used to disconnect from the target.

.. figure:: images/plecs_example/plecs_terminate_function_code.png
   :name: fig-plecs-example-plecs-terminate-function-code
   :scale: 40%
   :align: center
   :alt: Terminate function code.
   
   Terminate function code.

Running OPiL target
-----------------------

``OPiL target`` is executed by the embedded target. In this example, Windows is used to emulate the embedded target, and an executable file that runs ``OPiL target`` is created from a C program. In this C program, ``OPiL target`` is initialized and executed in an infinite loop.

The `main.c <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/examples/buck_target_win/src/main.c>`_ file located in ``examples/buck_target_win/src`` folder shows the ``main`` function of the C program used in this example.

First, the controller interface is initialized, in line 24. Then, ``OPiL target`` is initialized in lines 27-44, where the proper callbacks are set. These callbacks provide interfaces to the controller and for communication.

The ``while(1)`` loop executes ``OPiL target``. In this example, ``opiltargetConnectToHost`` is called (line 55), which blocks execution until a connection is received from the host. When a connection is received, ``opiltargetExchangeDataHost`` can be executed, which is responsible for executing the state machine of ``OPiL target``, where data is received from the host, the controller is executed, and data is sent back (line 59). This is executed until a disconnection is detected. After disconnection, the connection is closed on the target side, by calling ``opiltargetDisconnectFromHost`` (line 64). 

Interfacing OPiL target and the controller
----------------------------------------------

``OPiL target`` and the controller are interfaced through the controller interface (see :ref:`sec-c-implementation-ctlrif`).

The actual controller is implemented in the `buckcontrol.h <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/examples/buck_target_win/src/buckcontrol.h>`_ and `buckcontrol.c <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/examples/buck_target_win/src/buckcontrol.c>`_ files, also located in ``examples/buck_target_win/src``.

During initialization of ``OPiL target``, the two functions defined in `buckcontrol.h <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/examples/buck_target_win/src/buckcontrol.h>`_ are given as parameters to the controller interface (see line 24 in `main.c <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/examples/buck_target_win/src/main.c>`_). Note that these functions have the signature defined by the controller interface (see `ctlrif.h <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/ctlrif/ctlrif.h>`_).

The ``initialize`` function is called once when ``OPiL target`` receives a new connection, which means that a new simulation is about to start. This can be used to initialize the controller. In this example, the initialize function is just used to print some debug information to the console.

The ``control`` function is called once everytime when ``OPiL target`` receives new measurements and simulation data. The control algorithm should be implemented in this function. The ``control`` function receives four pointers. The first two pointers, denoted as ``ms`` and ``sd`` in `buckcontrol.h <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/examples/buck_target_win/src/buckcontrol.h>`_, indicate where the measurements and simulation data are located at. These pointers should be read-only. Inside the controller function, they are cast to ``stypesMeasurements_t`` and ``stypesSimData_t`` types (see line 75 and 76 of `buckcontrol.c <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/examples/buck_target_win/src/buckcontrol.c>`_). The other two pointers, denoted as ``ctl`` and ``ctlrdata``, indicate where the control signals and controller data should be written to. These pointers should be write-only. Inside the controller function, they are cast to ``stypesControl_t`` and ``stypesControllerData_t`` types (see line 77 and 78 of `buckcontrol.c <https://gitlab.rhrk.uni-kl.de/lrs/opil/-/blob/b29691371acbfc7340fdde8920fc2e774a8a307f/examples/buck_target_win/src/buckcontrol.c>`_).


